#!/usr/bin/python3

# Author: Nilesh Ghule <nilesh@sunbeaminfo.com>
# admission -> csvio.py
# Date: 19-Apr-2020 8:00 AM

import csv

from config import cfg


def load_degrees_csv():
    di = {}
    file = open(cfg['degrees_file'], 'r')
    reader = csv.reader(file)
    header = ['degree']
    i = 1
    for row in reader:
        di[i] = row
        i = i + 1
    return di


def load_students_csv():
    li = []
    file = open(cfg['students_file'], 'r')
    reader = csv.reader(file)
    header = ['id', 'name', 'rank_a', 'rank_b', 'rank_c', 'degree', 'marks', 'pref', 'course', 'center', 'paid', 'reported', 'prn']

    def convert_field(field, value):
        if field=='id' or field=='rank_a' or field=='rank_b' or field=='rank_c' or field=='pref' or field=='reported':
            return int(value)
        if field=='marks' or field=='paid':
            return float(value)
        return value

    for row in reader:
        rowobj = {}
        for key,value in zip(header,row):
            rowobj[key] = convert_field(key,value)
        rowobj['preferences'] = {}
        li.append(rowobj)
    return li


def load_preferences_csv(students):
    file = open(cfg['preferences_file'], 'r')
    reader = csv.reader(file)
    header = ['stud_id', 'pref', 'course', 'center']

    def convert_field(field, value):
        if field=='stud_id' or field=='pref':
            return int(value)
        return value

    for row in reader:
        rowobj = {}
        for key,value in zip(header,row):
            rowobj[key] = convert_field(key,value)
        stud = next(s for s in students if s['id']==rowobj['stud_id'])
        stud['preferences'][rowobj['pref']] = rowobj


def load_courses_csv():
    di = {}
    file = open(cfg['courses_file'], 'r')
    reader = csv.reader(file)
    header = ['id', 'course', 'fees', 'section']

    def convert_field(field, value):
        if field=='id':
            return int(value)
        if field=='fees':
            return float(value)
        return value

    for row in reader:
        rowobj = {}
        for key,value in zip(header,row):
            rowobj[key] = convert_field(key,value)
        rowobj['eligibilities'] = {}
        rowobj['capacities'] = {}
        di[rowobj['course']] = rowobj
    return di


def load_eligibilities_csv(courses):
    file = open(cfg['eligibilities_file'], 'r')
    reader = csv.reader(file)
    header = ['course', 'degree', 'min_marks']

    def convert_field(field, value):
        if field=='min_marks':
            return float(value)
        return value

    for row in reader:
        rowobj = {}
        for key,value in zip(header,row):
            rowobj[key] = convert_field(key,value)
        course = courses[rowobj['course']]
        course['eligibilities'][rowobj['degree']] = rowobj['min_marks']


def load_centers_csv():
    di = {}
    file = open(cfg['centers_file'], 'r')
    reader = csv.reader(file)
    header = ['center', 'name', 'address', 'coordinator', 'password']

    for row in reader:
        rowobj = {}
        for key,value in zip(header,row):
            rowobj[key] = value
        rowobj['capacities'] = {}
        di[rowobj['center']] = rowobj
    return di


def load_capacities_csv(courses, centers):
    li = []
    file = open(cfg['capacities_file'], 'r')
    reader = csv.reader(file)
    header = ['center', 'course', 'seats', 'filled']

    def convert_field(field, value):
        if field=='seats' or field=='filled':
            return int(value)
        return value

    for row in reader:
        rowobj = {}
        for key,value in zip(header,row):
            rowobj[key] = convert_field(key,value)
        course = courses[rowobj['course']]
        center = centers[rowobj['center']]
        course['capacities'][rowobj['center']] = len(li)
        center['capacities'][rowobj['course']] = len(li)
        li.append(rowobj)
    return li


def save_students_csv(students):
    header = ['id', 'name', 'rank_a', 'rank_b', 'rank_c', 'degree', 'marks', 'pref', 'course', 'center', 'paid', 'reported', 'prn']
    file = open(cfg['students_file'], 'w')
    writer = csv.DictWriter(file, fieldnames=header, extrasaction='ignore')
    writer.writerows(students)
    print("students saved: ", len(students))


def save_capacities_csv(capacities):
    header = ['center', 'course', 'seats', 'filled']
    file = open(cfg['capacities_file'], 'w')
    writer = csv.DictWriter(file, fieldnames=header, extrasaction='ignore')
    writer.writerows(capacities)
    print("capacities saved: ", len(capacities))


def save_preferences_csv(students):
    header = ['stud_id', 'pref', 'course', 'center']
    file = open(cfg['preferences_file'], 'w')
    writer = csv.DictWriter(file, fieldnames=header, extrasaction='ignore')
    cnt = 0
    for s in students:
        writer.writerows(s['preferences'].values())
        cnt = cnt + len(s['preferences'])
    print("preferences saved: ", cnt)
