#!/usr/bin/python3

# Author: Nilesh Ghule <nilesh@sunbeaminfo.com>
# admission -> admissionsystem.py
# Date: 19-Apr-2020 7:59 AM

import csvio


degrees = dict()
students = list()
courses = dict()
centers = dict()
capacities = list()


def load_admission_system():
    global  degrees
    global students
    global courses
    global centers
    global capacities

    students = csvio.load_students_csv()
    csvio.load_preferences_csv(students)
    courses = csvio.load_courses_csv()
    csvio.load_eligibilities_csv(courses)
    centers = csvio.load_centers_csv()
    capacities = csvio.load_capacities_csv(courses, centers)


def print_seats():
    for cap in capacities:
        print(cap)


def print_allocated_seats():
    cnt = 0
    for cap in capacities:
        print(cap)
        cnt += cap['filled']
    print("allocated seats: ", cnt)


def save_allocation_result():
    csvio.save_students_csv(students)
    csvio.save_capacities_csv(capacities)


def save_students():
    csvio.save_students_csv(students)


# common functions
def register_new_student():
    s = dict()
    while True:
        s['id'] = int(input('student id: '))
        try:
            stud = next(st for st in students if st['id'] == s['id'])
        except:
            break
        print('Student id already exists.')
    s['name'] = input('student name: ')
    for no,degree in degrees:
        print(no, " - ", degree)
    s['degree'] = degrees[int(input('student degree: '))]
    s['marks'] = float(input('student marks: '))
    s['rank_a'] = s['rank_b'] = s['rank_c'] = -1
    s['pref'] = 0
    s['reported'] = 0
    s['paid'] = -1
    s['course'] = s['center'] = s['prn'] = 'NA'
    s['preferences'] = {}
    students.append(s)


def print_allocated_students(course='', center=''):
    allocated_studs = filter(lambda s: s['pref'] > 0, students)
    if course != '':
        allocated_studs = filter(lambda s: s['course'] == course, allocated_studs)
    if center != '':
        allocated_studs = filter(lambda s: s['center'] == center, allocated_studs)
    sorted_studs = sorted(allocated_studs, key=lambda s: (s['center'], s['course']))
    cnt = 0
    for s in sorted_studs:
        print(s)
        cnt = cnt + 1
    print("allocated students: ", cnt)


def print_paid_students(course='', center=''):
    paid_studs = filter(lambda s: s['paid'] > 0, students)
    if course != '':
        paid_studs = filter(lambda s: s['course'] == course, paid_studs)
    if center != '':
        paid_studs = filter(lambda s: s['center'] == center, paid_studs)
    sorted_studs = sorted(paid_studs, key=lambda s: (s['center'], s['course']))
    cnt = 0
    for s in sorted_studs:
        print(s)
        cnt = cnt + 1
    print("paid students: ", cnt)


def print_reported_students(course='', center=''):
    reported_studs = filter(lambda s: s['reported'] != 0, students)
    if course != '':
        reported_studs = filter(lambda s: s['course'] == course, reported_studs)
    if center != '':
        reported_studs = filter(lambda s: s['center'] == center, reported_studs)
    sorted_studs = sorted(reported_studs, key=lambda s: (s['center'], s['course']))
    cnt = 0
    for s in sorted_studs:
        print(s)
        cnt = cnt + 1
    print("paid students: ", cnt)


# student functions
def is_student_eligible_for_course(s,c):
    if c['eligibilities'].has_key(s['degree']):
        min_marks = c['eligibilities'][s['degree']]
        if s['marks'] >= min_marks:
            return True
    return False


def print_student_eligible_courses(s):
    for c in courses.values():
        if is_student_eligible_for_course(s, c):
            temp = c
            temp.pop('eligibilities')
            print(temp)


def print_all_centers(print_capacities=False):
    for c in centers.values():
        temp = c
        if not print_capacities:
            temp.pop('capacities')
        print(temp)


def print_course_centers(course):
    for c in centers.values():
        if c['center'] in courses[course]['capacities']:
            temp = c
            temp.pop('capacities')
            print(temp)


def input_student_preferences(s):
    pref_count = int(input('how many preferences (max 10): '))
    for pref in range(1,pref_count+1):
        print_student_eligible_courses(s)
        course = input('enter course: ')
        print_course_centers()
        center = input('enter center: ')
        s['pref'] = pref
        s['course'] = course
        s['center'] = center
    print('Your preferences are locked.')


def print_student_allocated_center(s):
    temp = centers[s['center']]
    # temp.pop('capacities')
    print('allocated center: ', temp)

    temp = courses[s['course']]
    # temp.pop('eligibilities')
    print('allocated course: ', temp)


def update_student_payment(s):
    if s['pref'] == 0:
        print('No center is allocated to you.')
        return

    print_student_allocated_center(s)

    print('\nCourse Fees:', courses[s['course']]['fees'])
    print('Paid amount:', s['paid'])

    s['paid'] = float(input('Enter total amount paid: '))


def save_student_preferences():
    csvio.save_preferences_csv(students)


# admin functions
def print_all_courses(print_eligibilities=False):
    for c in courses.values():
        temp = c
        if not print_eligibilities:
            temp.pop('eligibilities')
        print(temp)


def print_all_students():
    sorted_studs = sorted(students, key=lambda s: (s['center'], s['course']))
    for s in sorted_studs:
        print(s)


def update_student_ranks():
    id = int(input('enter student id: '))
    for s in students:
        if s['id'] == id:
            s['rank_a'] = int(input('rank A (-1 if NA): '))
            s['rank_b'] = int(input('rank B (-1 if NA): '))
            s['rank_c'] = int(input('rank C (-1 if NA): '))
            return
    print('Student Not Found.')


def seat_allocation_round1():

    def to_be_allocated(s, section, pref_num):
        return s['preferences'].get(pref_num) is not None and courses[s['preferences'][pref_num]['course']]['section'] == section \
            and s['pref'] == 0 and s['paid'] >= 0.0

    def allocate_pref(stud, pref_num):
        stud_pref = stud['preferences'][pref_num]
        course = courses[stud_pref['course']]
        capacity = capacities[ course['capacities'][stud_pref['center']] ]
        if capacity['filled'] < capacity['seats']:
            capacity['filled'] += 1
            stud['pref'] = pref_num
            stud['course'] = stud_pref['course']
            stud['center'] = stud_pref['center']
            return True
        return False

    sections = ['A', 'B', 'C']
    ranks = ['rank_a', 'rank_b', 'rank_c']
    for pref in range(1, 11):
        for section,rank in zip(sections,ranks):
            students.sort(key=lambda s: s[rank])
            for stud in students:
                if to_be_allocated(stud, section, pref):
                    allocate_pref(stud, pref)


def seat_allocation_round2():
    for s in students:
        if s['pref'] != 0:
            # mark student as unpaid, so that he will be out of system
            if s['paid'] == 0.0:
                s['paid'] = -1.0
            # decrement seat allocated for corresponding preference
            stud_pref = s['preferences'][s['pref']]
            course = courses[stud_pref['course']]
            capacity = capacities[course['capacities'][stud_pref['center']]]
            capacity['filled'] -= 1
            # remove his allocation
            s['pref'] = 0
            s['course'] = "NA"
            s['center'] = "NA"

    seat_allocation_round1()


def generate_prn():
    sorted_studs = sorted(students, key=lambda s: (s['center'], s['course'], s['name']))
    prev_course = ''
    prev_center = ''
    srno = 1
    for s in sorted_studs:
        if s['reported'] != 0 and s['paid'] >= courses[s['course']]['fees']:
            if s['course'] != prev_course or s['center'] != prev_center:
                srno = 1
            s['prn'] = s['center'] + '_' + s['course'] + '_' + str(srno)
            srno = srno + 1
            prev_course = s['course']
            prev_center = s['center']


def print_admitted_students(course='', center=''):
    admitted_studs = filter(lambda s: s['prn'] != 'NA', students)
    if course != '':
        admitted_studs = filter(lambda s: s['course'] == course, admitted_studs)
    if center != '':
        admitted_studs = filter(lambda s: s['center'] == center, admitted_studs)
    sorted_studs = sorted(admitted_studs, key=lambda s: (s['center'], s['course']))
    cnt = 0
    for s in sorted_studs:
        print(s)
        cnt = cnt + 1
    print("admitted students: ", cnt)


# center coordinator functions
def update_students_reported_status(center):
    course = input('enter course: ')
    print_allocated_students(course=course, center=center)
    print('enter student ids of reported students (-1 to stop).')
    stud_id = 0
    while stud_id != -1:
        stud_id = int(input('reported student id: '))
        if stud_id == -1:
            break
        try:
            stud = next(s for s in students if s['id'] == stud_id and s['center'] == center and s['course'] == course)
            stud['reported'] = 1
            print('student marked as reported: ', stud)
        except:
            print('Student is Not Found or Not allocated to given to center/course.')


def print_center_courses(center):
    for c in courses.values():
        if c['course'] in centers[center]['capacities']:
            temp = c
            # temp.pop('eligibilities')
            print(temp)
