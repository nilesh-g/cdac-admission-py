#!/usr/bin/python3

# Author: Nilesh Ghule <nilesh@sunbeaminfo.com>
# admission -> main.py
# Date: 19-Apr-2020 8:00 AM

import admissionsystem as admsys


def admin_menu():
    while True:
        print("\n0. return to main menu\n1. list courses & eligibilities\n2. list centers & capacities\n3. list all students\n4. update student ranks\n5. allocate centers (round1)\n6. allocate centers (round2)\n7. list allocated students\n8. list paid students\n9. list reported students\n10. generate prn numbers\n11. list admitted students")
        option = int(input("input option: "))
        if option == 0:
            break
        elif option == 1:
            admsys.print_all_courses(print_eligibilities=True)
        elif option == 2:
            admsys.print_all_centers(print_capacities=True)
        elif option == 3:
            admsys.print_all_students()
        elif option == 4:
            admsys.update_student_ranks()
        elif option == 5:
            admsys.seat_allocation_round1()
            admsys.save_allocation_result()
        elif option == 6:
            admsys.seat_allocation_round2()
            admsys.save_allocation_result()
        elif option == 7:
            admsys.print_allocated_students()
        elif option == 8:
            admsys.print_paid_students()
        elif option == 9:
            admsys.print_reported_students()
        elif option == 10:
            admsys.generate_prn()
            admsys.save_students()
        elif option == 11:
            admsys.print_admitted_students()
        else:
            print("invalid option")


def coordinator_menu(center):
    center_id = center['center']
    print('Logged in Center: ', center_id)
    while True:
        print("\n0. return to main menu\n1. list courses\n2. list students\n3. update reported status\n4. list admitted students")
        option = int(input("input option: "))
        if option == 0:
            break
        elif option == 1:
            admsys.print_center_courses(center_id)
        elif option == 2:
            admsys.print_allocated_students(center=center_id)
        elif option == 3:
            admsys.update_students_reported_status(center_id)
        elif option == 4:
            admsys.print_admitted_students(center=center_id)
        else:
            print("invalid option")


def student_menu(s):
    while True:
        print("\n0. return to main menu\n1. list courses\n2. list centers\n3. give preferences\n4. print allocated center\n5. update payment details")
        option = int(input("input option: "))
        if option == 0:
            break
        elif option == 1:
            admsys.print_student_eligible_courses(s)
        elif option == 2:
            admsys.print_all_centers(print_capacities=False)
        elif option == 3:
            admsys.input_student_preferences(s)
            admsys.save_student_preferences()
        elif option == 4:
            admsys.print_student_allocated_center(s)
        elif option == 5:
            admsys.update_student_payment(s)
            admsys.save_students()
        else:
            print("invalid option")


def sign_in():
    username = input('enter username: ')
    password = input('password: ')
    if username == 'admin' and password == 'admin':
        admin_menu()
    elif username in admsys.centers and password == admsys.centers[username]['password']:
        coordinator_menu(admsys.centers[username])
    elif username.isdigit():
        stud_id = int(username)
        try:
            stud = next(s for s in admsys.students if s['id'] == stud_id and s['name'] == password)
            student_menu(stud)
        except Exception as e:
            print(e)
            print('Invalid student login.')
    else:
        print('Invalid login.')


if __name__ == '__main__':
    admsys.load_admission_system()
    while True:
        print("\n0. exit\n1. register new student\n2. sign in")
        option = int(input("input option: "))
        if option == 0:
            break
        elif option == 1:
            admsys.register_new_student()
        elif option == 2:
            sign_in()
        else:
            print("invalid option")

