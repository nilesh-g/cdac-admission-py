#!/usr/bin/python3

# Author: Nilesh Ghule <nilesh@sunbeaminfo.com>
# admission -> config.py
# Date: 19-Apr-2020 7:59 AM

cfg = {
    'degrees_file': 'data/degrees.txt',
    'students_file': 'data/students.csv',
    'preferences_file': 'data/preferences.csv',
    'courses_file': 'data/courses.csv',
    'centers_file': 'data/centers.csv',
    'eligibilities_file': 'data/eligibilities.csv',
    'capacities_file': 'data/capacities.csv'
}
